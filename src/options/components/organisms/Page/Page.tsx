import React from "react";
import cx from "classnames";
import Question from "@/options/components/molecules/Question/Question";
import DarkModeToggle from "@/options/components/molecules/DarkMode/DarkMode";
import Provider from "../../molecules/Provider/Provider";

const Page: React.FC = () => {
  return (
    <div className={cx("w-[500px] mt-14 space-y-6")}>
      <div className="flex justify-between">
        <h1 className="text-3xl font-bold tracking-tight">Options</h1>
        <DarkModeToggle />
      </div>

      <Question />
      <Provider />
    </div>
  );
}

export default Page;
