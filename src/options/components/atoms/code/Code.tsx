const Code = ({ text }) => {
  return (
    <code
      className={`border m-0.5 inline-block text-xs
    rounded px-1 py-0.5 bg-gray-50
    dark:text-gray-800 shadow-sm mr-1`}>
      {text}
    </code>
  );
}

export default Code;
