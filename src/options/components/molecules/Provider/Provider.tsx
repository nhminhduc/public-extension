import React, { useState } from "react";
import cx from "classnames";
import Section from "@/commonComponents/Section/Section";

import s from "./Provider.module.css";
import DropdownMenu from "@/commonComponents/dropdownMenu/DropdownMenu";

const providers = [
  { label: "Chat GPT", value: "chatGPT" },
  { label: "Open AI", value: "openAI" },
];

const models = [
  { label: "Default (GPT-3.5)", value: "default" },
  { label: "Legacy (GPT-3.5) [Deprecating soon]", value: "legacy" },
  { label: "GPT-4", value: "gpt-4" },
];

const Provider: React.FC = () => {
  const [provider, setProvider] = React.useState<string>("chatGPT");
  const [activeDropdown, setActiveDropdown] = useState<string | null>(null);

  const handleProvidersSelect = (value: string) => {
    setProvider(value);
  };

  const handleDropdownToggle = (dropdownId: string) => {
    setActiveDropdown((current) => (current === dropdownId ? null : dropdownId));
  };

  return (
    <Section
      title={"AI Provider"}
      className="text-inherit mt-2"
      titleClassName="text-lg font-medium leading-6 border-b pb-2"
    >
      <DropdownMenu
        options={providers}
        id="providers-dropdown"
        onSelect={handleProvidersSelect}
        defaultSelectedValue={"chatGPT"}
        title="Provider"
        className="py-2"
        buttonClassName={cx(s.dropdownButton)}
        contentClassName="w-full overflow-hidden"
        itemClassName="font-normal text-sm"
        isActive={activeDropdown === "providers-dropdown"}
        onToggle={() => handleDropdownToggle("providers-dropdown")}
      />

      {provider === "chatGPT" && (
        <DropdownMenu
          options={models}
          id="models-dropdown"
          defaultSelectedValue={"default"}
          title="Model"
          className="py-2"
          buttonClassName={cx(s.dropdownButton)}
          contentClassName="w-full overflow-hidden"
          itemClassName="font-normal text-sm"
          isActive={activeDropdown === "models-dropdown"}
          onToggle={() => handleDropdownToggle("models-dropdown")}
        />
      )}
      {provider === "openAI" && (
        <div className="py-2">
          <label>API Key</label>
          <input
            type="text"
            className={"px-4 py-1.5 rounded focus:border-[#10a37f] w-full"}
          />
        </div>
      )}

      <div className="mt-3">
        <button type="submit" className={s.saveButton}>
          Save
        </button>
      </div>
    </Section>
  );
};

export default Provider;
