import { useDarkMode } from "@/context/DarkModeContext";

function DarkModeToggle() {
  const darkModeContext = useDarkMode();

  if (!darkModeContext) {
    return null;
  }

  const { darkMode, setDarkMode } = darkModeContext;

  return (
    <button
      className="focus:outline-none"
      onClick={() => setDarkMode(!darkMode)}
    >
      {darkMode ? 'Dark Mode' : 'Light Mode'}
    </button>
  );
}

export default DarkModeToggle;
