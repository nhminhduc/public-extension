import React, { useRef } from "react";
import cx from "classnames";
import Section from "@/commonComponents/Section/Section";
import Code from "@/options/components/atoms/code/Code";
import { InfoIcon } from "@primer/octicons-react";
import Button from "@/commonComponents/button/Button";
import TextArea from "@/commonComponents/textArea/TextArea";

import s from "./Question.module.css";

const Question: React.FC = () => {

  const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
  }

  const textAreaContent = `Your output should use the following template:

  ### Summary
  
  ### Facts
  
  - [Emoji] Bulletpoint
  
  Your task is to summarize the text I give you in up to seven bulletpoints and start with a short summary. Pick a good matching emoji for every bullet point. Reply in {{SELECTED_LANGUAGE}}. The url to extract facts from is this: {{URL}}. If the url has a paywall or no content use this text: {{CONTENT}}`

  return (
    <Section title={"Question"} className="text-inherit" titleClassName="text-lg font-medium leading-6 border-b pb-2">
      <form className="h-auto pt-3" onSubmit={onSubmit}>
        <TextArea
          id={"question"}
          name="question"
          altText={"Token Count: 105"}
          inputClassName={cx(s.textAreaInput, "dark:text-gray-900 sm:text-sm")}
          rows={13}
        >{textAreaContent}</TextArea>
        <div className="flex flex-wrap items-center">
          <p className="basis-full mt-3 text-gray-600 italic dark:text-gray-200 text-xs text-primary-gray-500">
            The question that should be send to ChatGPT. You can use the following placeholders:
            <Code text={`{{TITLE}}`} />
            <Code text={`{{CONTENT}}`} />
            <Code text={`{{URL}}`} />
            <Code text={`{{SELECTED_LANGUAGE}}`} />
            <Button className="inline-flex items-center align-middle hover:bg-gray-300 rounded-full">
              <InfoIcon size={16} className="dark:text-gray-50 text-gray-900 font-light" />
            </Button>
          </p>
        </div>
        <div className="my-3">
          <button className={s.saveButton}>Save</button>
          <button className={s.resetButton}>Reset</button>
        </div>
      </form>

    </Section>
  );
}

export default Question;
