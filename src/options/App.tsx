import React from 'react';
import cx from 'classnames';
import Page from './components/organisms/Page/Page';
import { DarkModeProvider } from '@/context/DarkModeContext';

const App: React.FC = () => {
  return (
    <DarkModeProvider>
      <div className="overflow-auto w-screen h-screen bg-gray-50 text-gray-900 dark:border-[#333440] dark:bg-[#333440] dark:text-gray-50">
        <div className={cx("flex justify-center px-4 items-center text-base")}>
          <Page />
        </div>
      </div>
    </DarkModeProvider >
  );
};

export default App;
