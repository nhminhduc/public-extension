import Browser from "webextension-polyfill";

const useCurrentTabId = async () => {
  const tabs = await Browser.tabs.query({ active: true, currentWindow: true });
  return (tabs[0]);
};

export default useCurrentTabId;
