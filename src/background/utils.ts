export function validURL(url: string) {
  return /^https?:\/\//i.test(url || '');
}
