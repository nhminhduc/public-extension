import Browser from 'webextension-polyfill';
import { validURL } from './utils';

let iframePosition = { x: 0, y: 0 };

Browser.runtime.onMessage.addListener(async (message) => {
  console.log(message);
  switch (message.type) {
    case 'OPEN_OPTIONS_PAGE': {
      Browser.runtime.openOptionsPage()
    }
  }
});

Browser.action.onClicked.addListener(async (tab) => {
  const { id } = tab

  if (!id) {
    return;
  }

  await Browser.tabs.sendMessage(id, { type: 'TOGGLE_IFRAME' });
})

Browser.tabs.onUpdated.addListener(async (tabId, changeInfo, tab) => {
  const isValidUrl = validURL(tab.url?.toString() || '');
  if (!isValidUrl) {
    await Browser.action.disable(tabId);
  }

  if (changeInfo.status === 'complete' && isValidUrl) {
    await Browser.tabs.sendMessage(tabId, { type: 'GET_PAGE_CONTENT', tabId });
    await Browser.tabs.sendMessage(tabId, { type: 'OPEN_WEB_SUMMARY' });
  }
});

Browser.runtime.onInstalled.addListener(async (details) => {
  if (details.reason === 'install') {
    Browser.tabs.create({ "url": "/popup/popup.html" });
  }
})

Browser.tabs.onRemoved.addListener(async (tabId) => {
  Browser.storage.local.remove([`pageContent-${tabId}`])
});
