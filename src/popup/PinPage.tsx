import React, { useEffect, useRef, useState } from "react";
import cx from "classnames";
import Button from "@/commonComponents/button/Button";
import { ArrowRightIcon, GearIcon } from "@primer/octicons-react";
import Confetti from "react-confetti";

import s from "./App.module.css";

const PinPage: React.FC = () => {
  const [height, setHeight] = useState<number>();
  const [width, setWidth] = useState<number>();
  const confetiRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (confetiRef.current) {
      setHeight(window.innerHeight);
      setWidth(window.innerWidth);
    }
  }, []);

  return (
    <div className="confetti-wrap" ref={confetiRef}>
      <div className={cx("mx-auto max-w-3xl text-center")}>
        <h2 className="text-lg font-semibold leading-8 tracking-tight text-primary-500">Thank you for installing</h2>
        <h1 className="text-4xl font-bold tracking-tight text-gray-900 dark:text-gray-50">ChatGPT » summarize everything</h1>
        <h3 className="mx-auto my-6 max-w-xl text-lg leading-8 text-gray-600 dark:text-gray-300 prose prose-primary">
          {`Ready! Summarize any webpage or Youtube video with one click using ChatGPT Suite! Contact us for any `}
          <a target="_blank" rel="noreferrer" href="#" className="text-primary-500">questions or feedback</a>
          {`. Happy summarizing!`}
        </h3>
        <div className="mt-8 flex items-center justify-center gap-x-6">
          <Button className={cx(s.optionsButton,)} href="/options/options.html">
            <GearIcon size={16} className="mr-1" />
            Options
          </Button>
          <Button className="text-base font-semibold leading-7" href="https://wikipedia.org/wiki/ChatGPT">
            Try it now
            <ArrowRightIcon size={16} />
          </Button>

        </div>
      </div>
      <Confetti numberOfPieces={150} width={width} height={height} />
    </div>
  );
}

export default PinPage;
