import React, { useEffect } from 'react';
import cx from 'classnames';
import Welcome from './Welcome';
import PinPage from './PinPage';
import { DarkModeProvider } from '@/context/DarkModeContext';

const App: React.FC = () => {
  const [isPinned, setIsPinned] = React.useState(false);

  const handlePin = async () => {
    const userSettings = await chrome.action.getUserSettings();
    setIsPinned(userSettings.isOnToolbar);
  }

  //Check if extension is pinned 
  useEffect(() => {
    handlePin();

    const interval = setInterval(() => {
      handlePin();
    }, 300);

    return () => {
      clearInterval(interval);
    }
  }, []);

  return (
    <DarkModeProvider>
      <div className={cx("container mx-auto text-base h-full")}>
        <div className="py-24 px-6 sm:px-6 lg:px-8">
          {isPinned ? <PinPage /> : <Welcome />}
        </div>
      </div>
    </DarkModeProvider>
  );
};

export default App;
