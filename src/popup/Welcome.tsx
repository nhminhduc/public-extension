import React from "react";
import cx from "classnames";

const Welcome: React.FC = () => {

  return (
    <div className={cx("flex flex-col items-center w-full h-full")}>
      <h2 className="text-lg font-semibold leading-8 tracking-tight text-[#10A37F]">Thank you for installing</h2>
      <h1 className="text-4xl font-bold tracking-tight text-gray-900 dark:text-gray-50">ChatGPT » summarize everything</h1>
      <h3 className="mx-auto my-6 max-w-xl text-lg leading-8 text-gray-600 dark:text-gray-300">Pin this extension to your toolbar to get started.</h3>
    </div>
  );
}

export default Welcome;
