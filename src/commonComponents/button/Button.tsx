import React, { MouseEvent, useCallback } from "react";
import cx from 'classnames';
import { ReactNode } from 'react';

import s from './Button.module.css';

interface ButtonProps {
  children?: ReactNode;
  className?: string;
  onClick?(event: MouseEvent<HTMLButtonElement | HTMLAnchorElement>): void;
  href?: string;
}
const Button = ({ children, className, href, onClick }: ButtonProps) => {
  const handleOnClick = useCallback((event: MouseEvent<HTMLButtonElement | HTMLAnchorElement>) => {
    if (onClick) {
      onClick(event);
    }
  }, [onClick])

  const buttonClassName = cx(s.button,
    `focus:outline-none focus:ring
    hover:text-gray-300 focus:outline-none
    active:outline-none`,
    className);

  if (href) {
    return (
      <a href={href} className={buttonClassName} onClick={handleOnClick}>
        {children}
      </a>
    )
  }
  return (
    <button
      className={buttonClassName}
      onClick={handleOnClick}>
      {children}
    </button>
  )
}

export default Button;
