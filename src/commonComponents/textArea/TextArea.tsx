// create text input area component
import cx from "classnames";
import RequiredFieldLabelMarking from "./RequiredFieldLabelMarking";
import { UseFormRegister, useFormContext } from "react-hook-form";

interface TextAreaProps {
  className?: string;
  id: string;
  label?: string;
  name: string;
  placeholder?: string;
  rows?: number;
  requried?: boolean;
  inputClassName?: string;
  altText?: string;
  children?: React.ReactNode;
};

const TextArea = ({
  className,
  inputClassName,
  id,
  label,
  placeholder,
  rows,
  altText,
  requried,
  children
}: TextAreaProps) => {
  return (
    <div className={cx(className)}>
      {label &&
        (<label htmlFor={id} className="text-lg font-medium leading-6 border-b pb-2 mb-3">
          <h2>{label}</h2>
        </label>)
      }
      {requried && <RequiredFieldLabelMarking />}
      <textarea className={cx("block w-full px-3 py-2", inputClassName)} id={id} placeholder={placeholder} rows={rows}>
        {children}
      </textarea>
      <p className="py-0.5 px-2 text-right text-xs text-gray-600 italic dark:text-gray-200 w-full flex justify-end">{altText}</p>
    </div>
  )
}

export default TextArea;;
