const RequiredFieldLabelMarking = () => {
  return (<span className="before:content-['*'] before:text-red-500">
    <span className="sr-only">Required field</span>
  </span>)
}

export default RequiredFieldLabelMarking;;
