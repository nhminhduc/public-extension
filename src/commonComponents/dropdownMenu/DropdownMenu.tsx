import React, { useState, useRef } from "react";
import cx from "classnames";
import { ReactNode } from "react";
import DropdownItem from "./DropdownItem";
import { ChevronDownIcon } from '@primer/octicons-react'

import Button from "../button/Button";

import s from "./Dropdown.module.css";

interface DropdownMenuProps {
  children?: ReactNode;
  className?: string;
  contentClassName?: string;
  options: { label: string; value: string }[];
  id: string;
  defaultSelectedValue?: string;
  onSelect?: (value: string) => void;
  title?: string;
  buttonClassName?: string;
  itemClassName?: string;
  isActive?: boolean;
  onToggle?: () => void;
}

const DropdownMenu = ({
  className,
  contentClassName,
  options,
  id,
  defaultSelectedValue = options[0]?.value,
  onSelect,
  title,
  buttonClassName,
  itemClassName,
  isActive = false,
  onToggle
}: DropdownMenuProps) => {

  const [selectedValue, setSelectedValue] = useState<string>(defaultSelectedValue);
  const [filteredOptions, setFilteredOptions] = useState(options);
  const inputRef = useRef<HTMLInputElement>(null);

  const toggleMenuButtonClick = () => {
    onToggle?.();
  };

  const handleOptionSelect = (value: string) => {
    setSelectedValue(value);
    onSelect?.(value);
    onToggle?.();
    if (inputRef.current) {
      inputRef.current.value = "";
      setFilteredOptions(options);
    }
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
    console.log(event.key);
    if (event.key === "Escape") {
      onToggle?.();
    } else if (event.key === "Enter") {
      handleOptionSelect(filteredOptions[0].value);
    } else {
      const key = event.key.toLowerCase();
      const optionsStartingWithKey = options.filter((option) =>
        option.label.toLowerCase().startsWith(key)
      );
      if (optionsStartingWithKey.length > 0) {
        setSelectedValue(optionsStartingWithKey[0].value);
      }
    }
  };

  const content: ReactNode = (
    <div
      className={cx(
        s.dropdownContent,
        "flex whitespace-nowrap",
        contentClassName,
        { hidden: !isActive },
      )}
      onKeyDown={handleKeyDown}
    >
      {options.map((option) => (
        <DropdownItem
          key={option.value}
          value={option.value}
          selected={selectedValue === option.value}
          onSelect={handleOptionSelect}
          className={cx("flex justify-between text-base", { ["font-bold bg-primary-500"]: selectedValue === option.value }, itemClassName)}
        >
          {option.label}
          {selectedValue === option.value && <span className="ml-2">&#10003;</span>}
        </DropdownItem>
      ))}
    </div>
  );

  return (
    <div className={cx(`relative inline-block`, className)}>
      {title && (
        <div>{title}</div>
      )}
      <Button
        className={cx(s.dropdownButton, "flex justify-between items-center text-left px-4 py-1.5 rounded focus:border-[#10a37f] w-full", buttonClassName)}
        aria-controls={id}
        aria-expanded={isActive}
        onClick={toggleMenuButtonClick}
      >
        {options.find((option) => option.value === selectedValue)?.label || "Select"}
        <ChevronDownIcon size={16} className="ml-1" />
      </Button>
      {content}
    </div>
  );
};

export default DropdownMenu;
