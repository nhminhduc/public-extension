import React from 'react';
import cx from 'classnames';

export interface DropdownItemProps {
  children: React.ReactNode;
  value: string;
  selected?: boolean;
  className?: string;
  onSelect?: (value: string) => void;
}

const DropdownItem = ({
  children,
  value,
  selected = false,
  className,
  onSelect,
}: DropdownItemProps) => {
  const handleClick = () => {
    onSelect?.(value);
  };

  return (
    <div
      className={cx('py-2 px-4 hover:bg-[#10a37f]', className)}
      onClick={handleClick}
    >
      {children}
    </div>
  );
};

export default DropdownItem;
