import { ReactNode } from "react";
import cx from "classnames";

type CardProps = {
  children: ReactNode;
  className?: string;
  title?: string;
};

const Card = ({ children, className, title }: CardProps) => {
  return (
    <div
      className={cx(className, "px-4 py-1")}
    >
      {title && <h3 className="text-lg font-medium mb-2">{title}</h3>}
      {children}
    </div>
  );
};

export default Card;
