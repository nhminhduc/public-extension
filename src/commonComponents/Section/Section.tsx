import React from "react";
import cx from "classnames";

interface SectionProps {
  title?: string;
  children?: React.ReactNode;
  className?: string;
  titleClassName?: string;
}
const Section = ({ title, children, className, titleClassName }: SectionProps) => {

  return (
    <div className={cx("flex flex-col h-full text-gray-50 overflow-visible rounded", className)}>
      {title && <h1 className={titleClassName}>{title}</h1>}
      {children}
    </div>
  );
}

export default Section;
