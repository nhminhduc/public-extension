import '@/assets/base.scss'
import { render } from 'react-dom';
import App from './App';

render(<App />, document.getElementById('root'));
