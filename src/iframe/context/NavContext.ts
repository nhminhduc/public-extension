import { createContext } from 'react';

type NavContextType = {
  isSummarizeButtonActive: boolean;
  setIsSummarizeButtonActive: (value: boolean) => void;
  isChatButtonActive: boolean;
  setIsChatButtonActive: (value: boolean) => void;
  loading: boolean;
  setLoading: (value: boolean) => void;
};

const NavContext = createContext<NavContextType>({
  isSummarizeButtonActive: true,
  setIsSummarizeButtonActive: () => { },
  isChatButtonActive: false,
  setIsChatButtonActive: () => { },
  loading: false,
  setLoading: () => { },
});

export default NavContext;
