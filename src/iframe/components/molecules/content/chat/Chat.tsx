import TextArea from "@/commonComponents/textArea/TextArea";
import { RocketIcon } from "@primer/octicons-react";

const ChatContent = () => {
  return (
    <div className="flex flex-col h-full overflow-hidden">
      <div className="flex-1 h-full">{` `}</div>
      <input
        placeholder="Send a message."
        className="mx-2 pl-2 text-base w-auto h-10 rounded text-gray-900 bg-gray-50 dark:text-gray-50 dark:bg-gray-700 focus-visible:outline-none"
        id={"message"}
        name={"message"} />
      <RocketIcon size={24} className="absolute bottom-11 right-4" />
      <div className="px-3 pb-3 pt-2 text-center text-xs text-gray-600 dark:text-gray-300">
        <span>{`Free Research Preview. ChatGPT may produce inaccurate information about people, places, or facts. `}
          <a href="https://help.openai.com/en/articles/6825453-chatgpt-release-notes" target="_blank" rel="noreferrer" className="underline">ChatGPT May 3 Version</a>
        </span>
      </div>
    </div>
  );
}

export default ChatContent;
