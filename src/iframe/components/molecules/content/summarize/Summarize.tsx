import Card from '@/commonComponents/card/Card';
import useCurrentTabId from '@/utils/useCurrentTabId';
import { ArticleData } from '@extractus/article-extractor';
import { CopyIcon, SyncIcon, ThumbsdownIcon, ThumbsupIcon } from '@primer/octicons-react';
import { useEffect, useState } from 'react';
import Browser from 'webextension-polyfill';
import cx from 'classnames';

function SummarizeContent() {
  const [pageContent, setPageContent] = useState<string[]>([]);
  const [pageTitle, setPageTitle] = useState<string | undefined>('');
  const currentTabId = useCurrentTabId();

  const emojis = ['🌟', '🔥', '👀', '💡', '🚀', '💻', '📝', '📚', '👨‍💻', '👩‍💻', '🌞', '🌈'];

  const getHTMLContent = async () => {
    return await currentTabId
      .then(({ id }) => Browser.storage.local.get(`pageContent-${id}`)
        .then(({ [`pageContent-${id}`]: pageContent }) => (pageContent as ArticleData)))
  };

  const stripHtml = (html) => {
    const regex = /<[^>]+>/g;
    return html.replace(regex, '');
  }


  function separateParagraphToParts(paragraph: string): string[] {
    const words = paragraph.split(' ');

    // If the paragraph has fewer than ten words, just return it as a single-element array
    if (words.length <= 10) {
      return [paragraph];
    }

    const wordsPerPart = Math.ceil(words.length / 10);
    const parts: string[] = [];

    for (let i = 0; i < 10; i++) {
      let part = '';
      for (let j = 0; j < wordsPerPart; j++) {
        const wordIndex = i * wordsPerPart + j;
        if (wordIndex >= words.length) {
          break;
        }
        part += words[wordIndex] + ' ';
      }
      parts.push(part.trim());
    }

    return parts;
  }

  useEffect(() => {
    getHTMLContent().then((article) => {
      if (article) {
        setPageContent(separateParagraphToParts(stripHtml(article.content)));
        setPageTitle(article.title);
      }
    });
  }, []);

  return (
    <div className="flex flex-col">
      <div className={cx("self-end")}>
        <SyncIcon size={16} className="mx-1.5" />
        <ThumbsupIcon size={16} className="mx-1.5" />
        <ThumbsdownIcon size={16} className="mx-1.5" />
        <CopyIcon size={16} className="mx-1.5" />
      </div>
      <article className="flex flex-col break-words max-w-none text-gray-900 dark:text-gray-50 overflow-auto">
        <Card title={"Summary"} className="flex flex-col -mt-3">
          <p className="text-sm text-gray-900 dark:text-gray-300">{pageTitle}</p>
        </Card>
        <Card title={"Facts"} className="flex flex-col mt-6">
          <ul className="marker:text-grey-300 list-disc pl-6">
            {pageContent.map((paragraph, index) => {
              const randomEmoji = emojis[Math.floor(Math.random() * emojis.length)];
              return (
                <li key={index} className="prose-sm text-sm text-gray-900 dark:text-gray-300 my-[1px] pl-1 leading-7">
                  {`${randomEmoji} ${paragraph}`}
                </li>
              )
            })}
          </ul>
        </Card>
      </article>
    </div>
  );
}

export default SummarizeContent;
