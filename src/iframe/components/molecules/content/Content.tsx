import React, { useContext } from "react";
import cx from "classnames";
import NavContext from "@/iframe/context/NavContext";
import SummarizeContent from "./summarize/Summarize";
import ChatContent from "./chat/Chat";
import Spinner from "@/commonComponents/spinner/Spinner";

const Content: React.FC = () => {
  const { loading, isSummarizeButtonActive, isChatButtonActive } = useContext(NavContext);

  if (loading) {
    return (
      <div className="flex justify-center mt-4 h-full w-full">
        <Spinner />
      </div>
    )
  }

  return (
    <div
      className={cx("flex flex-col justify-between h-full")}
    >
      <div className="mt-4 h-full">
        {isSummarizeButtonActive && (
          <SummarizeContent />
        )}
        {isChatButtonActive && (
          <ChatContent />
        )}
      </div>
    </div>
  );
}

export default Content;
