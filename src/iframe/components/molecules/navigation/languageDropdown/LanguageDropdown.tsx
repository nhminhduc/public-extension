import React, { useState } from "react";
import DropdownMenu from "@/commonComponents/dropdownMenu/DropdownMenu";

import { languages } from "@/iframe/utils/languages";

const LanguageDropdown = () => {
  const [selectedLanguage, setSelectedLanguage] = useState<string>("en");
  const [activeDropdown, setActiveDropdown] = useState<boolean>(false);

  const handleLanguageSelect = (value: string) => {
    setSelectedLanguage(value);
  };

  const handleDropdownToggle = () => {
    setActiveDropdown((current) => !current);
  };

  return (
    <DropdownMenu
      options={languages}
      id="language-dropdown"
      onSelect={handleLanguageSelect}
      defaultSelectedValue={selectedLanguage}
      className="px-2 py-1.5"
      contentClassName="right-1 w-auto"
      isActive={activeDropdown}
      onToggle={handleDropdownToggle}
      buttonClassName="text-gray-900 dark:text-gray-50 bg-gray-50 dark:bg-transparent"
    />
  );
};

export default LanguageDropdown;
