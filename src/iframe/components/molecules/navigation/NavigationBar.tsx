import React, { useContext } from "react";
import { useState } from "react";
import cx from 'classnames';
import Button from "@/commonComponents/button/Button";
import LanguageDropdown from "./languageDropdown/LanguageDropdown";
import NavContext from "@/iframe/context/NavContext";

import s from "./NavigationBar.module.css";

const NavigationBar: React.FC = () => {
  const { isSummarizeButtonActive, isChatButtonActive, setIsSummarizeButtonActive, setIsChatButtonActive } = useContext(NavContext);

  return (
    <nav className="sticky dark:bg-[#333440] shadow-md dark:shadow-none dark:border-b border-gray-200 bg-white shrink-0 border-y px-2 flex justify-between items-center">
      <div>
        <Button
          className={cx(s.navbarButton, "text-gray-900 dark:text-gray-50 hover:dark:text-gray-100", { "border-b-[2px] border-green-700": isSummarizeButtonActive })}
          onClick={() => {
            setIsSummarizeButtonActive(true);
            setIsChatButtonActive(false);
          }}
        >
          Summarize
        </Button>
        <Button
          className={cx(s.navbarButton, "text-gray-900 dark:text-gray-50 hover:dark:text-gray-100", { "border-b-[2px] border-green-700": isChatButtonActive })}
          onClick={() => {
            setIsSummarizeButtonActive(false);
            setIsChatButtonActive(true);
          }}
        >
          Chat
        </Button>
      </div>
      <LanguageDropdown />
    </nav>
  )
}

export default NavigationBar;
