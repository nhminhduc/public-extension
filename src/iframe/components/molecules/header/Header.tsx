import cx from "classnames";
import Browser from 'webextension-polyfill';
import { RocketIcon, GearIcon, XIcon } from '@primer/octicons-react';
import { useCallback } from "react";
import useDrag from "@/iframe/hooks/useDrag";

import s from "./Header.module.css";

const Header = () => {
  const { ref, isDragging } = useDrag();

  const closeIframe = useCallback(() => {
    Browser.tabs.query({ active: true, currentWindow: true }).then((tabs) => {
      const currentTabId = tabs[0].id;
      if (!currentTabId) {
        return;
      }
      Browser.tabs.sendMessage(currentTabId, { type: 'TOGGLE_IFRAME' });
    });
  }, [])

  const openOptions = useCallback(() => {
    Browser.runtime.sendMessage({ type: 'OPEN_OPTIONS_PAGE' });
  }, []);

  return (
    <div
      ref={ref}
      id="header"
      className={cx(`cursor-grab shrink-0 flex justify-between content-center flex-wrap h-12 w-full z-50
      bg-[#343541] text-[#d9d9e3] dark:text-[#eaeaea] dark:bg-[#333440] dark:border-b`, {
        ["cursor-grabbing"]: isDragging,
        ["cursor-grab"]: !isDragging,
      })}
    >
      <div className="flex items-center">
        <RocketIcon size={16} className="ml-4" />
        <h1 className="px-2 text-base m-0 ml-1">ChatGPT » Summarize everything</h1>
      </div>
      <div className="flex content-center mr-1">
        <button className={cx(s.headerButton, "focus:ring-1 focus:outline-none hover:text-gray-50")} onClick={openOptions}>
          <GearIcon size={16} />
          <span className="sr-only">Options</span>
        </button>
        <button className={cx(s.headerButton, "focus:ring-1 focus:outline-none hover:text-gray-50")} onClick={closeIframe}>
          <XIcon size={24} />
          <span className="sr-only">Close</span>
        </button>
      </div>
    </div>
  );
}

export default Header;
