import React, { createContext, useState } from "react";
import cx from "classnames";
import Header from "@/iframe/components/molecules/header/Header";
import NavigationBar from "@/iframe/components/molecules/navigation/NavigationBar";
import Content from "@/iframe/components/molecules/content/Content";
import NavContext from "@/iframe/context/NavContext";

const Page: React.FC = () => {
  const [isSummarizeButtonActive, setIsSummarizeButtonActive] = useState(true);
  const [isChatButtonActive, setIsChatButtonActive] = useState(false);
  const [loading, setLoading] = useState(false);

  const contextValue = {
    isSummarizeButtonActive,
    isChatButtonActive,
    loading,
    setIsSummarizeButtonActive,
    setIsChatButtonActive,
    setLoading
  };

  return (
    <NavContext.Provider value={contextValue}>
      <div className={cx("flex flex-col h-full text-gray-50 rounded")}>
        <Header />
        <NavigationBar />
        <Content />
      </div>
    </NavContext.Provider>
  );
}

export default Page;
