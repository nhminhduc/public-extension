import { useState, useRef, useEffect } from "react";
import Browser from "webextension-polyfill";

type MouseEventWithMovement = MouseEvent & {
  movementX: number;
  movementY: number;
};

type UseDragResult = {
  ref: React.MutableRefObject<HTMLDivElement | null>;
  isDragging: boolean;
};

function useDrag(): UseDragResult {
  const [isDragging, setIsDragging] = useState(false);
  const [initialPosition, setInitialPosition] = useState({ x: 0, y: 0 });
  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const handleMouseMove = (event: MouseEventWithMovement) => {
      setIsDragging(true);

      const { movementX, movementY } = event;
      const deltaX = movementX - initialPosition.x;
      const deltaY = movementY - initialPosition.y;
      Browser.tabs.query({ active: true, currentWindow: true }).then((tabs) => {
        const currentTab = tabs[0];
        Browser.tabs.sendMessage(currentTab.id as number, {
          type: 'MOUSE_DRAGGING',
          payload: {
            deltaX,
            deltaY
          }
        });
      });
    };

    const handleMouseUp = () => {
      setIsDragging(false);
      document.removeEventListener('mouseup', handleMouseUp);
      document.removeEventListener('mousemove', handleMouseMove);
    };

    const handleMouseDown = (event) => {
      setInitialPosition({ x: event.clientX, y: event.clientY });
      document.addEventListener('mousemove', handleMouseMove);
      document.addEventListener('mouseup', handleMouseUp);
    };

    const element = ref.current;

    if (element) {
      element.addEventListener('mousedown', handleMouseDown);
    }

    return () => {
      if (element) {
        element.removeEventListener('mousedown', handleMouseDown);
      }
    };
  }, []);

  return { ref, isDragging };
}

export default useDrag;
