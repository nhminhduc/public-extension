import React from 'react';
import cx from 'classnames';
import Page from './components/organisms/Page/Page';
import { DarkModeProvider } from '@/context/DarkModeContext';

const App: React.FC = () => {
  return (
    <DarkModeProvider>
      <div className={cx("h-[600px] w-[700px] dark:bg-[#333440] dark:text-gray-50 p-0 m-0 overflow-auto bg-white")}>
        <Page />
      </div>
    </DarkModeProvider>
  );
};

export default App;
