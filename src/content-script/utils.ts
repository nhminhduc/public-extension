import { extractFromHtml } from '@extractus/article-extractor';

export const getHTMLContent = async () => {
  const html = document.querySelector('html')?.outerHTML;
  const url = location.href;
  if (!html) {
    return;
  }

  const article = await extractFromHtml(html, url);

  return article;
}
