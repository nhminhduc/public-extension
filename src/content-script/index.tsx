import Browser from 'webextension-polyfill';
import { moveIframe, renderIframe, toggleIframe } from './iframe';
import { getHTMLContent } from './utils';

Browser.runtime.onMessage.addListener(async (message, _, sendResponse) => {
  const { type, tabId, payload } = message;
  console.log(message);

  switch (type) {
    case 'TOGGLE_IFRAME': {
      toggleIframe();
      break;
    }
    case 'OPEN_WEB_SUMMARY': {
      renderIframe();
      break;
    }
    case 'GET_PAGE_CONTENT': {
      if (!tabId) {
        return;
      }
      getHTMLContent().then((content) => Browser.storage.local.set({ [`pageContent-${tabId}`]: content }));
      break;
    }
    case 'MOUSE_DRAGGING': {
      const { deltaX, deltaY } = payload;
      moveIframe(deltaX, deltaY);
      break;
    }
  }
});
