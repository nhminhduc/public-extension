import Browser from "webextension-polyfill";

export async function renderIframe() {
  const myIframe = document.getElementById(`my-extension-iframe`) as HTMLIFrameElement;

  if (!myIframe) {
    const myIframe = document.createElement("iframe");

    myIframe.referrerPolicy = "unsafe-url";
    myIframe.setAttribute("class", "my-extension-iframe");
    myIframe.src = Browser.runtime.getURL("iframe/iframe.html");
    myIframe.id = `my-extension-iframe`;
    myIframe.style.all = "unset";
    myIframe.style.border = "0";
    myIframe.style.borderRadius = "8px";
    myIframe.style.top = "10px";
    myIframe.style.right = "10px";
    myIframe.style.zIndex = "999999999999";
    myIframe.style.height = "600px";
    myIframe.style.width = "700px";
    myIframe.style.boxSizing = "border-box";
    myIframe.style.position = "fixed";
    myIframe.style.transform = `translate(0px, 0px)`;
    myIframe.style.boxShadow = "rgba(0, 0, 0, 0.2) 0px 0px 10px 0px";
    myIframe.style.display = "none";

    document.body.appendChild(myIframe);
  }
}

export async function toggleIframe() {
  const myIframe = document.getElementById(`my-extension-iframe`) as HTMLIFrameElement;

  if (myIframe && myIframe.contentWindow) {
    if (myIframe.style.display === "none") {
      myIframe.style.display = "block";
    } else if (myIframe.style.display === "block") {
      myIframe.style.display = "none";
    }
  } else {
    renderIframe();
  }
}

export async function moveIframe(deltaX: number, deltaY: number) {
  const iframe = document.getElementById("my-extension-iframe") as HTMLIFrameElement;
  if (!iframe) {
    return;
  }

  const currentTransform = iframe.style.transform;
  const transformRegex = /translate\((?<x>-?\d+(?:\.\d+)?)px,\s*(?<y>-?\d+(?:\.\d+)?)px\)/;
  const match = currentTransform.match(transformRegex);

  if (match && match.groups) {
    const currentX = parseFloat(match.groups.x);
    const currentY = parseFloat(match.groups.y);

    const newX = currentX + deltaX;
    const newY = currentY + deltaY;

    iframe.style.transform = `translate(${newX}px, ${newY}px)`;
  }
}
