import archiver from 'archiver'
import autoprefixer from 'autoprefixer'
import * as dotenv from 'dotenv'
import esbuild from 'esbuild'
import postcssPlugin from 'esbuild-style-plugin'
import fs from 'fs-extra'
import process from 'node:process'
import tailwindcss from 'tailwindcss'
import fse from 'fs-extra'

dotenv.config()

const outdir = 'build'
const packagesDir = 'packages'
const appName = 'test'

const isDev = process.env.NODE_ENV === 'dev'

let buildConfig = {
  entryPoints: [
    'src/iframe/index.tsx',
    'src/background/index.ts',
    'src/content-script/index.tsx',
    'src/options/index.tsx',
    'src/popup/index.tsx',
  ],
  bundle: true,
  outdir: outdir,
  treeShaking: true,
  minify: true,
  drop: ['console', 'debugger'],
  legalComments: 'none',
  define: {
    'process.env.NODE_ENV': '"production"',
  },
  jsxFactory: 'h',
  jsxFragment: 'Fragment',
  jsx: 'automatic',
  loader: {
    '.png': 'dataurl',
    '.svg': 'dataurl',
  },
  plugins: [
    postcssPlugin({
      postcss: {
        plugins: [tailwindcss, autoprefixer],
      },
    }),
  ],
}

if (isDev) {
  buildConfig = { ...buildConfig, ...{ minify: false, drop: [] } }
}

async function deleteOldDir() {
  await fs.remove(outdir)
}

async function runEsbuild() {
  await esbuild.build(buildConfig)
}

async function zipFolder(dir) {
  const output = fs.createWriteStream(`${dir}.zip`)
  const archive = archiver('zip', {
    zlib: { level: 9 },
  })
  archive.pipe(output)
  archive.directory(dir, false)
  await archive.finalize()
}

async function copyFiles(entryPoints, targetDir) {
  await fs.ensureDir(targetDir)
  await Promise.all(
    entryPoints.map(async (entryPoint) => {
      await fs.copy(entryPoint.src, `${targetDir}/${entryPoint.dst}`)
    }),
  )
}

async function build() {
  await deleteOldDir()
  await runEsbuild()

  const commonFiles = [
    //{ src: 'build/contentScript/index.js', dst: 'contentScript.js' },
    { src: 'build/background/index.js', dst: 'background.js' },
    { src: 'build/iframe/index.js', dst: 'iframe/iframe.js' },
    { src: 'build/iframe/index.css', dst: 'iframe/iframe.css' },
    { src: 'src/iframe/index.html', dst: 'iframe/iframe.html' },
    { src: 'build/content-script/index.js', dst: 'content-script.js' },
    { src: 'src/options/index.html', dst: 'options/options.html' },
    { src: 'build/options/index.js', dst: 'options/options.js' },
    { src: 'build/options/index.css', dst: 'options/options.css' },
    { src: 'src/popup/index.html', dst: 'popup/popup.html' },
    { src: 'build/popup/index.js', dst: 'popup/popup.js' },
    { src: 'build/popup/index.css', dst: 'popup/popup.css' },
    { src: 'src/assets/icon.webp', dst: 'icon.png' },
  ]

  // chromium
  await copyFiles(
    [...commonFiles, { src: 'src/manifest.json', dst: 'manifest.json' }],
    `./${outdir}/chromium`,
  )

  await zipFolder(`./${outdir}/chromium`)
  await copyFiles(
    [
      {
        src: `${outdir}/chromium.zip`,
        dst: `${appName}chromium.zip`,
      },
    ],
    `./${packagesDir}`,
  )

  await copyFiles(
    [
      {
        src: `${outdir}/chromium`,
        dst: `./chromium`,
      },
    ],
    `./${packagesDir}`,
  )

  console.log('Build success.')
}

build()
